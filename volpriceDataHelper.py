# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 14:01:35 2022

@author: liuyf
"""


import pandas as pd
import pymssql
from datetime  import datetime

class VolPriceDataHelper(object):
    def __init__(self):
        pass
    
    #中证500指数行情数据
    def get_idx_bar_data(self,begt,endt,code_idx):
        # 用于读取指数的市场行情信息
        begt = str(datetime.strptime(begt,'%Y-%m-%d')+pd.Timedelta(days=-360))[:10]#为计算MA250
        endt = str(datetime.strptime(endt,'%Y-%m-%d')+pd.Timedelta(days=30))[:10]#向前多拿一天，生成信号的时候要拿下一交易日信号，如果回测日期最后一天非交易日就会出问题
        conn=pymssql.connect(
            server='datayunwai.go-goal.net',
            port='2433',
            user='select',
            password='zyyx.4567!',
            database='MONGO_STOCK_DB')
        sql='''
        select cast(tdate as datetime) as trade_date,
            index_code,
            chg_rate,
            topen,
            tclose,
            amount,
            volume
        from ts_idx_quote_daily
        where tdate between '{0}' and '{1}'
            and index_code in ('{2}')
        '''.format(begt,endt,code_idx)
        rlt=pd.read_sql(sql,conn)
        rlt.sort_values(by='trade_date',inplace=True)
        rlt['trade_date'] = rlt['trade_date'].apply(lambda x: datetime.strftime(x,'%Y-%m-%d %H:%M:%S')[:10])
        rlt.reset_index(drop=True,inplace=True)
        conn.close()
        return rlt
    
    def get_test_bar_data(self,bar_data,trade_time='open'):
        if trade_time=='open':
            bar_data = bar_data[['trade_date','index_code','topen']].copy()
            return bar_data
        elif trade_time=='close':
            bar_data = bar_data[['trade_date','index_code','tclose']].copy()
            return bar_data
        else:
            print('trade_time input error.')
        
    