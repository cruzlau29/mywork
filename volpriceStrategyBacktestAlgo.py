# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 14:26:18 2022

@author: liuyf
"""
from volpriceDataHelper import VolPriceDataHelper
from volpriceRelationIndicatorCalculator import VolPriceIndicatorCalculator
from volpriceStrategyWarehouse import VolPriceStrategyWarehouse
from timeAlteringStrategyBacktestPlatform import TimeAlteringStrategyBacktestPlatform

class VolPriceStrategyBacktestAlgo(object):
    def __init__(self,begt,endt,asset_code,fast_period,slow_period,dea_period,ma_fast_period,ma_slow_period,trade_time):
        self.dh = VolPriceDataHelper()
        self.ind = VolPriceIndicatorCalculator()
        self.wh = VolPriceStrategyWarehouse()
        self.bt = TimeAlteringStrategyBacktestPlatform()
        
        self.begt = begt
        self.endt = endt
        self.asset_code = asset_code
        self.trade_time = trade_time
        
        self.fast_period = fast_period
        self.slow_period = slow_period
        self.dea_period = dea_period
        self.ma_fast_period = ma_fast_period
        self.ma_slow_period = ma_slow_period
        self.bar_data = self.dh.get_idx_bar_data(begt,endt,asset_code)
        pass 
    
    def generate_strategy1_trade_position_info(self,begt,endt):
        indicators = self.ind.get_vol_price_indicators(self.bar_data, self.begt, self.endt, self.fast_period, self.slow_period, self.dea_period,self.ma_fast_period,self.ma_slow_period)
        #macd动量择时
        # posDF = self.wh.macdIdxSgy1(indicators)#取策略信号，这里取的是盘后信号
        #ma简单双均线
        posDF = self.wh.maIdxSgy1(indicators,begt,endt)
        return posDF
        
    #流程函数
    def generate_backtest_result(self):
        posDF = self.generate_strategy1_trade_position_info(self.begt,self.endt)
        hisBarData = self.bt.get_asset_his_bar_data(self.bar_data, self.begt, self.endt,trade_time=self.trade_time)
        rets = self.bt.gen_asset_and_sgy_rets(hisBarData, posDF)#策略、指数、超额的收益序列，非净值序列
        net_vals = self.bt.gen_nav_curves(rets)#生成策略、指数、超额的净值序列并绘图
        self.bt.gen_return_statistics(net_vals)
        
        