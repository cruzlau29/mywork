# -*- coding: utf-8 -*-
"""
Created on Fri Jun 24 10:21:01 2022

@author: liuyf
"""
import pandas as pd
import numpy as np
from datetime import datetime

from sklearn.linear_model import LinearRegression

from factorDataHelper import DataHelper


##############################################################################################
# 回归时若未证明不经过原点，则默认有截距
# 待修改：股票只删除缺失值大于20的证券
##############################################################################################


class StyleFactorCalculator(object):
    def __init__(self,begt,endt):
        self.begt = begt
        self.endt = endt
        self.lr = LinearRegression(fit_intercept=True)
        self.dh = DataHelper()
        pass
    
    
    #市值因子的三级因子LnCap, MidCap
    def calc_ln_size_factor(self,size_weighted=True):
        # 取数
        mvData = self.dh.get_market_size_daily(self.begt,self.endt)
       
        # 对数市值LnCap:流通市值的自然对数
        size_df = mvData.copy()
        sizeFactors = size_df.sort_values(by=['trade_date','stock_code']).reset_index(drop=True)
        sizeFactors['tcap'] = sizeFactors.groupby('stock_code',as_index=False)['tcap'].fillna(method='ffill')
        sizeFactors['LnCap'] = np.log(sizeFactors['tcap'])
        
        #中市值因子MidCap:首先取Size因子暴露的立方，然后以加权回归的方式对Size因子正交，最后进行去极值和标准化处理。中市值效应反映中市值证券表现相对高低市值证券的表现
        sizeFactors['cubicLnCap'] = sizeFactors['LnCap'] ** 3
        if size_weighted == True:# 总市值平方根加权回归
            sizeFactors['sizeWts'] = np.sqrt(sizeFactors['tcap'])
        else:#等权
            sizeFactors['sizeWts'] = 1
        y = sizeFactors['cubicLnCap'].values.reshape(-1,1)
        x = sizeFactors['LnCap'].values.reshape(-1,1)
        wts = sizeFactors['sizeWts']
        self.lr.fit(x,y,wts)
        sizeFactors['MidCap'] = self.lr.predict(x) - sizeFactors['cubicLnCap'].values.reshape(-1,1)#取残差的负数
        #对中市值因子进行中位数去极值和标准化处理
        sizeFactors['MidCap'] = self.winsorize_nx_median(sizeFactors['MidCap'])
        sizeFactors['MidCap'] = self.standardize(sizeFactors['MidCap'])
        
        # 整理
        sizeFactors = sizeFactors[['trade_date','stock_code','LnCap','MidCap']]
        return sizeFactors
    
    
    #Beta因子
    def calc_volatility_factor(self):
        regWinLen = 252
        betaHalfLifePeriod = 63
        dailyRetHalfLifePeriod = 42
        
        #beta需要用252期之前的数据进行回归，begt要往前取一年
        begin_date = (datetime.strptime(self.begt,'%Y-%m-%d') - pd.Timedelta(days=400)).strftime('%Y-%m-%d')
        end_date = self.endt
        
        #计算包含窗口的股票收益率
        stkChgR = self.dh.get_stock_change_rate_daily(begin_date,end_date).set_index(['trade_date','stock_code'])#读取股票涨跌幅数据
        # stkChgR = stkChgR.fillna(0)#填缺
        stkChgR['return'] = stkChgR['chg_rate']#股票收益
        stkRet = stkChgR.drop(columns='chg_rate').reset_index(drop=False)
        stkRet = pd.pivot_table(stkRet,values=['return'],columns=['stock_code'],index=['trade_date'])
        stkRet = stkRet.droplevel(level=0,axis=1)
        stkRet.reset_index(drop=False,inplace=True)
        stkRet = self.cut_dataframe_backward_a_number_of_days(stkRet,self.begt,regWinLen)#截取回归要用到的区间样本(252+len)
        
        # #计算包含窗口的基准收益率
        idxChgR = self.dh.get_index_change_rate_daily(begin_date,end_date,'000300').drop(columns='index_code')
        idxChgR = self.cut_dataframe_backward_a_number_of_days(idxChgR,self.begt,regWinLen)#截取回归要用到的区间样本(252+len)
        idxChgR = idxChgR.set_index('trade_date')#读取指数涨跌幅
        idxChgR = idxChgR.fillna(0)#填缺
        idxChgR['HS300'] = idxChgR['chg_rate']#指数收益
        idxRet = idxChgR.drop(columns='chg_rate').reset_index(drop=False)
        
        #计算股票对数收益率
        stkLnRet = np.log(stkRet.set_index('trade_date') + 1)
        stkLnRet = stkLnRet.reset_index(drop=False)
        
        
        #计算半衰期加权权数,与时间窗口唯一匹配
        sigmaBeta = 0.5**(1/betaHalfLifePeriod)
        sigmaDailyRet = 0.5**(1/dailyRetHalfLifePeriod)
        hlWtsBeta = np.array([(sigmaBeta ** (regWinLen-i)) for i in range(1,regWinLen+1)])#递增，时间越近权数越大
        hlWtsDailyRet = np.array([(sigmaDailyRet ** (regWinLen-i)) for i in range(1,regWinLen+1)]).reshape(-1,1)#递增，时间越近权数越大
        
        #滚动窗口遍历测试期间进行WLS回归，该窗口的回归结果beta_hat存入该窗口最后一天的日期作为当天的因子值
        # stkNameCols = stkRet.columns[1:].tolist()
        volFactors = pd.DataFrame(data=[],columns=['trade_date','stock_code','beta'])
        for i in range(len(stkRet)-regWinLen):
            Y = stkRet[i+1:i+regWinLen+1].copy()#用以第i次回归的区间数据，和时间窗口等长
            X = idxRet[i+1:i+regWinLen+1].copy()#用以第i次回归的区间数据，和时间窗口等长
            Y = Y.reset_index(drop=True)
            X = X.reset_index(drop=True)
            # Y = Y.fillna(0)
            Y = Y.dropna(axis=1)
            # X = X.fillna(0)
            curDate = Y.iloc[-1]['trade_date']
            curDateStr = datetime.strftime(curDate,'%Y-%m-%d %H:%M:%S')[:10]
            # print(curDateStr)
            Y = Y.drop(columns=['trade_date'])
            stkNameCols = Y.columns.tolist()
            X = X.drop(columns=['trade_date'])
            # print(X)
            self.lr.fit(X,Y.values,sample_weight=hlWtsBeta)#收益率对沪深300指数做加权回归
            #Beta
            curBetaArray = self.lr.coef_.reshape(-1,1)#当天所有证券的beta
            #beta回归残差波动率
            curResid = Y - self.lr.predict(X)
            curResidVol = curResid.std(axis=0).tolist()
            #日收益标准差v1
            curRetStd = (Y.values * hlWtsDailyRet).std(axis=0).reshape(-1,1)
            #日收益标准差v2
            YMean = np.mean(Y.values,axis=0)#Y是收益率
            curRetVar = (((Y.values - YMean) ** 2) * hlWtsDailyRet).sum(axis=0)
            curRetStd_v2 = np.sqrt(curRetVar)
            curRetStd_v2 = curRetStd_v2.reshape(-1,1)
            
            curVolDF = pd.DataFrame()
            curVolDF['trade_date'] = np.zeros(Y.shape[1])
            curVolDF['trade_date'] = curVolDF['trade_date'].replace(0,curDateStr)
            curVolDF['stock_code'] = stkNameCols
            curVolDF['beta'] = curBetaArray
            curVolDF['HistSigma'] = curResidVol
            curVolDF['DailyStd'] = curRetStd
            curVolDF['DailyStdV2'] = curRetStd_v2
            volFactors = pd.concat([volFactors,curVolDF])
            volFactors = volFactors.reset_index(drop=True)
        
        return volFactors
    
    
    
    ###################################################################################
    ##                                  工具函数                                      ## 
    ###################################################################################
    # 从起始日期前一定交易天数开始截取dataframe
    def cut_dataframe_backward_a_number_of_days(self,df,begt,back_day_num):
        begt = datetime.strptime(begt,'%Y-%m-%d')
        df = df.copy()
        begtIdx = df.loc[df['trade_date']==begt].index.values[0]
        winBegtIdx = begtIdx - back_day_num
        df = df.loc[winBegtIdx:,:]
        df = df.drop_duplicates()
        return df
    
    # 中位数去极值
    def winsorize_nx_median(self,df,n=5):
        mid = df.median()
        diffMid = abs(df-mid).median()
        upper_bound = mid + diffMid * n
        lower_bound = mid - diffMid * n
        df.loc[:] = np.where(df>upper_bound,upper_bound,df)
        df.loc[:] = np.where(df<lower_bound,lower_bound,df)
        return df.values
    
    def standardize(self,df):
        df = (df - df.mean())/df.std()
        return df.values
    
    
    
    

#因子模块测试    
if __name__=='__main__':
    fc = StyleFactorCalculator('2022-02-07','2022-02-08')
    # sizeFactors = fc.calc_ln_size_factor()
    volFactors = fc.calc_volatility_factor()
    
        
        
        