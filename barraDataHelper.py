# -*- coding: utf-8 -*-
"""
Created on Fri Jun 24 10:27:37 2022

@author: liuyf
"""
import pandas as pd
import pymssql


class DataHelper(object):
    def __init__(self):
        pass
    
    def get_market_size_daily(self,begt,endt):
        conn=pymssql.connect(
            server='114.80.161.232',
            port = '2433',
            user='select',
            password='select123',
            database='New_YZYQ',
            charset = 'utf8')
        sql='''
        select trade_date,stock_code,tcap from qt_stk_daily
        where trade_date between '{0}' and'{1}'
        '''.format(begt,endt)
        rlt=pd.read_sql(sql,conn)
        conn.close()
        return rlt
    
    def get_stock_change_rate_daily(self,begt,endt):
        conn=pymssql.connect(
            server='114.80.161.232',
            port = '2433',
            user='select',
            password='select123',
            database='New_YZYQ')
        sql='''
        select trade_date, stock_code, (change_rate/100) as chg_rate from qt_stk_daily
        where trade_date between '{0}' and'{1}'
        order by trade_date asc
        '''.format(begt,endt)
        rlt=pd.read_sql(sql,conn)
        conn.close()
        return rlt
    
    def get_index_change_rate_daily(self,begt,endt,idx_code):
        conn=pymssql.connect(
            server='114.80.161.232',
            port = '2433',
            user='select',
            password='select123',
            database='New_YZYQ')
        sql='''
        select trade_date, index_code, (change_rate/100) as chg_rate from qt_idx_daily
        where trade_date between '{0}' and'{1}'
        and index_code='{2}'
        order by trade_date asc
        '''.format(begt,endt,idx_code)
        rlt=pd.read_sql(sql,conn)
        conn.close()
        return rlt
    