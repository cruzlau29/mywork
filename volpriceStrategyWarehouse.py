# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 14:50:04 2022

@author: liuyf
"""
import pandas as pd
import numpy as np
from datetime import datetime
class VolPriceStrategyWarehouse(object):
    #1.要买入信号发出后次日的交易信号 2.交易信号对齐到产生信号的交易日
    def __init__(self):
        pass
    
    def macdIdxSgy1(self,inds):
        #MACD指数信号择时策略(信号发出次日开盘交易)
        #买入信号：
        #   今日DIF,今日DEA,今日MACD > 0
        #   昨日DIF < 昨日DEA
        #   今日DIF > 今日DEA
        #   今日MACD > 昨日MACD
        #卖出信号：
        #   昨日DIF > 昨日DEA
        #   今日DIF < 今日DEA
        #   今日MACD < 昨日MACD
        inds.dropna(inplace=True)
        
        inds['signal'] = np.nan
        # signal_dict['buySign'] = []
        # signal_dict['sellSign'] = []
        dateList = inds['trade_date'].values
        difList = inds['DIF'].values
        deaList = inds['DEA'].values
        macdList = inds['MACD'].values
        signal_df = inds[['trade_date','signal']]
        for i in range(1,len(inds)):
            curDIF = difList[i]
            curDEA = deaList[i]
            curMACD = macdList[i]
            lastDIF = difList[i-1]
            lastDEA = deaList[i-1]
            lastMACD = macdList[i-1]
            curDate = dateList[i]
            allowBuy = True
            #卖出信号(稍后需再处理成次日开盘下单信号)：
            conS1 = (lastDIF>lastDEA)
            conS2 = (curDIF<curDEA)
            conS3 = (curMACD<lastMACD)
            if conS1 & conS2 & conS3:
                allowBuy = False
                toTradeDate = self.fit_signal_into_next_trade_date(curDate,dateList)#先调整到下一天买，再判断下一天在不在交易日里，不在的话调整
                signal_df['signal'].loc[signal_df['trade_date']==toTradeDate] = 0
            #买入信号(稍后需再处理成次日开盘下单信号)：
            conB1 = (curDIF>0) and (curDEA>0) and (curMACD>0)#   DIF,DEA,MACD>0
            conB2 = (curDIF>curDEA)#   DIF>DEA
            conB3 = (lastDIF<lastDEA)#   DIFl<DEAl
            conB4 = (curMACD>lastMACD)#   MACD>MACDl
            if conB1 & conB2& conB3 & conB4 & allowBuy:
                toTradeDate = self.fit_signal_into_next_trade_date(curDate,dateList)#找事件信号发出的下一交易日作为持仓信号发出日
                signal_df['signal'].loc[signal_df['trade_date']==toTradeDate] = 1
            allowBuy = True
        
        #时间序列排序
        signal_df.sort_values(by='trade_date',inplace=True)
        signal_df.reset_index(drop=True,inplace=True)
        pos_df = self.turn_signal_df_into_position_info(signal_df)#事件信号转为持仓01信号
        return pos_df
        
    
    #简单双均线策略
    def maIdxSgy1(self,inds,begt,endt):#这里传入的指标数据从一年前开始就有了，所以用的时候直接算，在回测时截取相应的时间序列即可
        #MA5上穿MA10买入
        #MA5下破MA10卖出
        inds.dropna(inplace=True)
        inds['signal'] = np.nan
        
        dateList = inds['trade_date'].values
        maFastList = inds['MA_fast'].values
        maSlowList = inds['MA_slow'].values
        signal_df = inds[['trade_date','signal']]
        signal_df = signal_df[(signal_df['trade_date']>=begt) & (signal_df['trade_date']<=endt)]#在这里先确定了信号区间
        for i in range(1,len(inds)):
            curDate = dateList[i]
            curMA_fast = maFastList[i]
            curMA_slow = maSlowList[i]
            lastMA_fast = maFastList[i-1]
            lastMA_slow = maSlowList[i-1]
            allowBuy=True
            #卖出信号(原则上卖出规则宽于买入规则，卖出信号产生容易于买入信号)
            conS1 = lastMA_fast > lastMA_slow
            conS2 = curMA_fast <= curMA_slow
            if conS1 & conS2:
                allowBuy = False
                toTradeDate = self.fit_signal_into_next_trade_date(curDate,dateList)#先调整到下一天买，再判断下一天在不在交易日里，不在的话调整
                signal_df['signal'].loc[signal_df['trade_date']==toTradeDate] = 0    
            #买入信号
            conB1 = lastMA_fast < lastMA_slow
            conB2 = curMA_fast > curMA_slow
            if conB1 & conB2 & allowBuy:
                toTradeDate = self.fit_signal_into_next_trade_date(curDate,dateList)#找事件信号发出的下一交易日作为持仓信号发出日
                signal_df['signal'].loc[signal_df['trade_date']==toTradeDate] = 1
            allowBuy = True
                
        #时间序列排序
        signal_df.sort_values(by='trade_date',inplace=True)
        signal_df.reset_index(drop=True,inplace=True)
        pos_df = self.turn_signal_df_into_position_info(signal_df)#事件信号转为持仓01信号
        return pos_df
        
    
    
    
    
    
    ###################################################################################################
    ##                                          辅助函数                                              ##
    ###################################################################################################
    #找事件信号发出的下一交易日作为持仓信号发出日
    def fit_signal_into_next_trade_date(self,curDate,dateList):
        curDate = str(datetime.strptime(curDate,'%Y-%m-%d') + pd.Timedelta(days=1))[:10]#下一天
        while not curDate in dateList:
            curDate = str(datetime.strptime(curDate,'%Y-%m-%d') + pd.Timedelta(days=1))[:10]
            if curDate in dateList:
                break
        toTradeDate = curDate
        return toTradeDate
    
    
    #事件信号转为持仓01信号
    def turn_signal_df_into_position_info(self,signal_df):
        first_day_pos = signal_df['signal'].values[0]
        if first_day_pos != 1:#如果第一天不开仓，信号标记为0
            first_day_pos = 0
        signal_df.loc[0,'signal'] = first_day_pos
        pos_df = signal_df.fillna(method='ffill')
        return pos_df
        
            
    
        #策略买入信号：(三天买入期)
        #   量比 > 1.2 
        #   DIF > 0,
        #   DEA > 0,
        #   DIF_last < DEA_last,
        #   DIF > DEA
        #   MACD 增量>0
        #   MACD > 0
            
        #策略卖出信号：
        #   量比>=1且macd<=昨日macd
        #   DIF_last > DEA_last,
        #   DIF < DEA