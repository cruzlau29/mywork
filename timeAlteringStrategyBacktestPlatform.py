# -*- coding: utf-8 -*-
"""
Created on Wed Jun 15 16:07:00 2022

@author: liuyf
"""
import numpy as np
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['font.sans-serif'] = ['SimHei']
matplotlib.rcParams['font.family']='sans-serif'
matplotlib.rcParams['axes.unicode_minus'] = False

#个券/指数择时策略回测平台
class TimeAlteringStrategyBacktestPlatform(object):
    def __init__(self):
        pass
    
    #计算价格变动率和标的价格收益率
    def get_asset_his_bar_data(self,bar_data,begt,endt,trade_time='open'):
        if trade_time=='close':
            begt = str(datetime.strptime(begt,'%Y-%m-%d')+pd.Timedelta(days=-1))[:10]
            bar_data = bar_data[['trade_date','chg_rate']].copy()
            bar_data = bar_data[(bar_data['trade_date']>=begt) & (bar_data['trade_date']<=endt)]
            bar_data.reset_index(drop=True,inplace=True)
            bar_data.loc[0,'chg_rate'] = 0
            bar_data['asset_nv'] = bar_data['chg_rate'] + 1
            bar_data['asset_nv'] = bar_data['asset_nv'].cumprod(axis=0)
        elif trade_time=='open':
            #开盘价计算的涨幅包含了次日开盘的跳空
            bar_data = bar_data[['trade_date','topen']].copy()#拿开盘价交易的话第一天能取到当天开盘          
            bar_data = bar_data[(bar_data['trade_date']>=begt) & (bar_data['trade_date']<=endt)] 
            retList = []
            chgRList = []
            chgRList.append(0)
            retList.append(1)
            topenList = bar_data['topen'].values
            for i in range(1,len(bar_data)):
                curPrice = topenList[i]
                lastPrice = topenList[i-1]
                chg_rate = (curPrice - lastPrice) / lastPrice
                curRet = (1+chg_rate)
                chgRList.append(chg_rate)
                retList.append(curRet)
            bar_data['chg_rate'] = chgRList#回测时用来计算策略净值序列
            bar_data['asset_ret'] = retList#不择时的标的净值序列
        else:
            pass
        return bar_data
    
    def gen_asset_and_sgy_rets(self,hisBarData,posDF):
        barData = hisBarData.copy()
        posDF = posDF.copy()
        barData = pd.merge(barData,posDF,how='left',on='trade_date')
        #生成收益时间序列
        retList = []
        sgyChgRateList = []
        retList.append(1)
        sgyChgRateList.append(0)
        snList = barData['signal'].values
        chgRList = barData['chg_rate'].values
        for i in range(1,len(barData)):
            curSn = snList[i]
            curCR = chgRList[i]
            curSgyCR = curSn*curCR
            sgyChgRateList.append(curSgyCR)
            curRet = (1+curSgyCR)
            retList.append(curRet)
        barData['sgy_chg_rate'] = sgyChgRateList
        barData['sgy_ret'] = retList
        rets = barData[['trade_date','asset_ret','sgy_ret']]#策略和基准的收益序列
        rets['trade_date'] = rets['trade_date'].apply(lambda x: datetime.strptime(x,'%Y-%m-%d'))
        rets.set_index('trade_date',inplace=True)
        rets.columns = ['idx','sgy']
        #生成超额收益序列
        # rets['excess'] = (rets['sgy']-rets['idx']) + 1
        rets['excess'] = (1+rets['sgy']) / (1+rets['idx'])
        return rets
    
    def gen_nav_curves(self,rets):
        #生成净值时间序列
        ret_all = rets.copy()
        nav=ret_all.cumprod()
        # nav.reset_index(drop=False,inplace=True)
        nav.to_csv('NVs.csv')
    
        #绘制净值曲线
        fig=plt.figure(dpi=200)
        ax1=fig.add_subplot(111)
        ln11=ax1.plot(nav['idx'],label='基准指数')
        ln12=ax1.plot(nav['sgy'],label='择时策略')
        ax2=ax1.twinx()
        ln21=ax2.plot(nav['excess'],'r',label='对冲组合（右轴）')
        lns=ln11+ln12+ln21
        labs=[l.get_label() for l in lns]
        ax1.legend(lns,labs,loc=2)
        plt.savefig('NVcurve.jpg')
        return nav
        
    
    #收益统计    
    def gen_return_statistics(self,net_vals):
        net_vals = net_vals.copy()
        print(net_vals)
        seriesNameList = ['idx','sgy','excess']
        stats = pd.DataFrame()
        for curName in seriesNameList:
            nv = net_vals[curName]
            stats.loc['累计收益率', curName] = backtest_stats(nv).total_returns
            stats.loc['年化收益率', curName] = backtest_stats(nv).annualized_returns
            stats.loc['年化波动率', curName] = backtest_stats(nv).annualized_volatility
            stats.loc['最大回撤', curName] = backtest_stats(nv).Max_Drawback()
            stats.loc['夏普比率', curName] = backtest_stats(nv).Sharpe()
            stats.loc['Calmar比率', curName] = backtest_stats(nv).Calmar() 
        stats.to_csv('statastics.csv',encoding='utf_8_sig')
            


class backtest_stats():
    def __init__(self,data):#输入净值
        self.data = data
        self.total_returns = (data.iloc[-1]-1)
        # self.cir_in_year = 365//(data.index[1]-data.index[0]).days #每年的周期数
        self.num_years = (data.index[-1]-data.index[0]).days/365
        self.annualized_returns = ((data.iloc[-1]/data.iloc[0])**(1/self.num_years)-1)
        self.annualized_volatility = (data.pct_change().std()*(252**0.5))
    def Max_Drawback(self):
        net_value=self.data
        max_value=0
        df_tmp=pd.DataFrame(net_value)
        #df_tmp.columns=['value']
        df_tmp['drawback']=0
        for j in range(0,len(net_value),1):
            max_value=max(max(max_value, df_tmp.iloc[j,0]),1)
            df_tmp.iloc[j, 1]=1-df_tmp.iloc[j, 0]/max_value
            drawback=df_tmp['drawback'].max()
        return drawback
    def Sharpe(self):
        bench_pct=0.03
        sharpe = (self.annualized_returns-bench_pct)/self.annualized_volatility
        return sharpe
    def Calmar(self):
        clamar = self.annualized_returns/self.Max_Drawback()
        return clamar
        
        
        
        
        
        

    
    
    
    
        
    
        