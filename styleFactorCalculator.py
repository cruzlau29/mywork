# -*- coding: utf-8 -*-
"""
Created on Fri Jun 24 10:21:01 2022

@author: liuyf
"""
import pandas as pd
import numpy as np
from datetime import datetime

from sklearn.linear_model import LinearRegression

from factorDataHelper import DataHelper


##############################################################################################
# 待修改：股票只删除缺失值大于20的证券
         # 市值因子做回归时要截距项，beta应该不用？
##############################################################################################


class StyleFactorCalculator(object):
    def __init__(self,begt,endt):
        self.begt = begt
        self.endt = endt
        self.lr = LinearRegression(fit_intercept=False)
        self.dh = DataHelper()
        pass
    #----------------------------- Volatility ----------------------------- #
    #Beta因子
    def calc_volatility_factor(self):
        regWinLen = 252
        betaHalfLifePeriod = 63
        dailyRetHalfLifePeriod = 42
        
        #beta需要用252期之前的数据进行回归，begt要往前取一年
        begin_date = (datetime.strptime(self.begt,'%Y-%m-%d') + pd.Timedelta(days=-400)).strftime('%Y-%m-%d')
        end_date = self.endt
        
        #计算包含窗口的股票收益率
        stkChgR = self.dh.get_stock_change_rate_daily(begin_date,end_date).set_index(['trade_date','stock_code'])#读取股票涨跌幅数据
        # stkChgR = stkChgR.fillna(0)#填缺
        stkChgR['return'] = stkChgR['chg_rate']#股票收益
        stkRet = stkChgR.drop(columns='chg_rate').reset_index(drop=False)
        stkRet = pd.pivot_table(stkRet,values=['return'],columns=['stock_code'],index=['trade_date'])
        stkRet = stkRet.droplevel(level=0,axis=1)
        stkRet.reset_index(drop=False,inplace=True)
        stkRet = cut_dataframe_backward_a_number_of_days(stkRet,self.begt,regWinLen)#截取回归要用到的区间样本(252+len)
        
        # #计算包含窗口的基准收益率
        idxChgR = self.dh.get_index_change_rate_daily(begin_date,end_date,'000300').drop(columns='index_code')
        idxChgR = cut_dataframe_backward_a_number_of_days(idxChgR,self.begt,regWinLen)#截取回归要用到的区间样本(252+len)
        idxChgR = idxChgR.set_index('trade_date')#读取指数涨跌幅
        idxChgR = idxChgR.fillna(0)#填缺
        idxChgR['HS300'] = idxChgR['chg_rate']#指数收益
        idxRet = idxChgR.drop(columns='chg_rate').reset_index(drop=False)
        
        
        #计算半衰期加权权数,与时间窗口唯一匹配
        sigmaBeta = 0.5**(1/betaHalfLifePeriod)
        sigmaDailyRet = 0.5**(1/dailyRetHalfLifePeriod)
        hlWtsBeta = np.array([(sigmaBeta ** (regWinLen-i)) for i in range(1,regWinLen+1)])#递增，时间越近权数越大
        hlWtsDailyRet = np.array([(sigmaDailyRet ** (regWinLen-i)) for i in range(1,regWinLen+1)]).reshape(-1,1)#递增，时间越近权数越大
        
        #滚动窗口遍历测试期间进行WLS回归，该窗口的回归结果beta_hat存入该窗口最后一天的日期作为当天的因子值
        # stkNameCols = stkRet.columns[1:].tolist()
        volFactors = pd.DataFrame(data=[],columns=['trade_date','stock_code','beta'])
        for i in range(len(stkRet)-regWinLen):
            Y = stkRet[i+1:i+regWinLen+1].copy()#用以第i次回归的区间数据，和时间窗口等长
            X = idxRet[i+1:i+regWinLen+1].copy()#用以第i次回归的区间数据，和时间窗口等长
            Y = Y.reset_index(drop=True)
            X = X.reset_index(drop=True)
            # Y = Y.fillna(0)
            Y = Y.dropna(axis=1)
            # X = X.fillna(0)
            curDate = Y.iloc[-1]['trade_date']
            curDateStr = datetime.strftime(curDate,'%Y-%m-%d %H:%M:%S')[:10]
            # print(curDateStr)
            Y = Y.drop(columns=['trade_date'])
            stkNameCols = Y.columns.tolist()
            X = X.drop(columns=['trade_date'])
            # print(X)
            #多元WLS回归
            self.lr.fit(X,Y.values,sample_weight=hlWtsBeta)
            curBetaArray = self.lr.coef_.reshape(-1,1)#当天所有证券的beta
            curResid = Y.values - self.lr.predict(X)
            curResidVol = curResid.std(axis=0).reshape(-1,1)
            curRetStd = (Y.values * hlWtsDailyRet).std(axis=0).reshape(-1,1)
            
            curVolDF = pd.DataFrame()
            curVolDF['trade_date'] = np.zeros(Y.shape[1])
            curVolDF['trade_date'] = curVolDF['trade_date'].replace(0,curDateStr)
            curVolDF['stock_code'] = stkNameCols
            curVolDF['beta'] = curBetaArray
            curVolDF['HistSigma'] = curResidVol
            curVolDF['DailyStd'] = curRetStd
            print(curVolDF)
            volFactors = pd.concat([volFactors,curVolDF])
            volFactors = volFactors.reset_index(drop=True)
            
        
        return volFactors
    
    
    #对数市值因子,输入：市值
    def calc_ln_size_factor(self):
        size_df = self.dh.get_market_size_daily(self.begt,self.endt)
        size = size_df.groupby('stock_code').ffill()
        size['lnSize'] = np.log(size['tcap'])
        lnSize = size.drop(columns='tcap')
        return lnSize
    
    
    #----------------------------- Momentum ----------------------------- #
    # 短期反转
    def cal_short_term_reversal_factor(self):
        win = 21
        half_life = 5
        begin_date = (datetime.strptime(self.begt,'%Y-%m-%d') + pd.Timedelta(days=-40)).strftime('%Y-%m-%d')
        stkChgR = self.dh.get_stock_change_rate_daily(begin_date,self.endt)
        stkChgR['chg_rate'] = stkChgR['chg_rate'].fillna(0)
        
        # 计算对数收益率
        stkChgR['stkLnRet'] = np.log(1+stkChgR['chg_rate'])
        stkLnRet = stkChgR[['trade_date','stock_code','stkLnRet']]
        stockList = stkLnRet['stock_code'].values
        stockList = list(set(stockList))
        stkLnRet = pd.pivot_table(stkLnRet,index=stkLnRet['trade_date'],columns=stkLnRet['stock_code'])
        stkLnRet = stkLnRet.fillna(0)
        stkLnRet.columns = stkLnRet.columns.droplevel(0)
        stkLnRet = stkLnRet.sort_index()
        
        # 计算半衰期加权权数
        hlWts = calc_half_life_weights(win,half_life)
        hlWts = hlWts.reshape(-1,1)
        # hlWtsDiag = np.diag(hlWts)
        
        # 对数收益率矩阵的周期切片和半衰期相乘，并将周期切片内的值求和作为当日的加权累积对数收益率
        stRev = pd.DataFrame(columns=stkLnRet.columns)
        tradeDateList = stkLnRet.index.values
        for i in range(0,len(stkLnRet)-win+1):
            curRetWindow = stkLnRet.iloc[i:i+win]
            wtdLnRet = curRetWindow.values * hlWts
            wtdCumLnRet = wtdLnRet.sum(axis=0)   # 得到单日stRev切片
            wtdCumLnRet = pd.DataFrame([wtdCumLnRet],columns=stkLnRet.columns)
            stRev = stRev.append(wtdCumLnRet)
        stRev.index = tradeDateList[win-1:]
        stRev = stRev.loc[stRev.index>=self.begt]
        return stRev 
        
        
        
    
# 半衰期加权权数计算,输出半衰期权重array
def calc_half_life_weights(window_length,half_life_period):
    sigma = 0.5**(1/half_life_period)
    hlWts = np.array([(sigma ** (window_length-i)) for i in range(1,window_length+1)])
    return hlWts
    
    
    
#从起始日期前一定交易天数开始截取dataframe
def cut_dataframe_backward_a_number_of_days(df,begt,back_day_num):
    begt = datetime.strptime(begt,'%Y-%m-%d')
    df = df.copy()
    begtIdx = df.loc[df['trade_date']==begt].index.values[0]
    winBegtIdx = begtIdx - back_day_num
    df = df.loc[winBegtIdx:,:]
    df = df.drop_duplicates()
    return df


#因子模块测试    
if __name__=='__main__':
    fc = StyleFactorCalculator('2022-02-07','2022-07-08')
    volFactors = fc.cal_short_term_reversal_factor()
    # test = fc.calc_ln_size_factor()
        
        
        