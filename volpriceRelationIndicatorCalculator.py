# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 16:58:14 2022

@author: liuyf
"""

import pandas as pd


class VolPriceIndicatorCalculator(object):
    def __init__(self):
        pass
    
    #1. ExpMovingAvg
    # def build_EMA_series(self,bar_data,period=12):
    #     # close_series = bar_data[['trade_date','tclose']].copy()
    #     # rho = 2 / (period + 1)
    #     # sigma = (period-1) / (period+1)
    #     # closeList = close_series['tclose'].values
    #     # emaList = []
    #     # emaList.append(closeList[0])#首日ema
    #     # for i in range(1,len(close_series)):
    #     #     curEMA = rho * closeList[i] + sigma * emaList[i-1]
    #     #     emaList.append(curEMA)
    #     # emaSeries = pd.Series(emaList,index=close_series['trade_date'])
        
    #     close_series = bar_data[['trade_date','tclose']].copy()
    #     emaList = close_series['tclose'].ewm(span=period,adjust=False).mean().values
    #     dateList = close_series['trade_date'].values
    #     emaSeries = pd.Series(emaList,index=dateList)
    #     emaSeries.name = 'EMA_' + str(period)
    #     emaSeries.sort_values(axis='index',ascending=True,inplace=True)
    #     return emaSeries
    
    # #2. DIF:
    # def build_DIF_series(self,bar_data,fast_period,slow_period):
    #     emaFast = self.build_EMA_series(bar_data,period=fast_period)
    #     emaSlow = self.build_EMA_series(bar_data,period=slow_period)
    #     difSeries = emaFast - emaSlow
    #     difSeries.name = 'DIF'
    #     difSeries.sort_values(axis='index',ascending=True,inplace=True)
    #     return difSeries
    
    # #3. DEA
    # def build_DEA_series(self,bar_data,fast_period,slow_period,dea_period):
    #     # rho = 2 / (dea_period + 1)#2/10
    #     # sigma = (dea_period - 1) / (dea_period + 1)#8/10
    #     difSeries = self.build_DIF_series(bar_data,fast_period,slow_period)
    #     # deaList = []
    #     # deaList.append(difSeries[0])
    #     # difList = difSeries.values
    #     # for i in range(1,len(difSeries)):
    #     #     curDEA = rho * difList[i] + sigma * deaList[i-1]
    #     #     deaList.append(curDEA)
        
    #     deaList = difSeries.ewm(span=dea_period).mean().values
        
    #     deaSeries = pd.Series(deaList,index=difSeries.index)
    #     deaSeries.name = 'DEA'
    #     deaSeries.sort_values(axis='index',ascending=True,inplace=True)
    #     return deaSeries
    
    # #4. MACD
    # def build_MACD_series(self,bar_data,fast_period,slow_period,dea_period):
    #     difSeries = self.build_DIF_series(bar_data, fast_period, slow_period)
    #     deaSeries = self.build_DEA_series(bar_data, fast_period, slow_period, dea_period)
    #     macdSeries = 2 * (difSeries - deaSeries)
    #     macdSeries.name = 'MACD'
    #     macdSeries.sort_values(axis='index',ascending=True,inplace=True)
    #     return macdSeries
    
    #5. 均线MA(period>=5)
    # def build_ma_series(self,bar_data,periodList=[5,10]):
    #     close_series = bar_data['trade_date','tclose'].copy()
    #     for curPeriod in periodList:   
    #         periodStr = str(curPeriod)
    #         maName = "".join(['ma',periodStr])
            
    #         close_series[maName] = 0
    #         priceList = close_series['tclose'].values
    #         maList = close_series[maName].values
    #         for i in range(curPeriod-1,len(close_series)):
    #             maList[i] = sum(priceList[(i-curPeriod+1),i]) / curPeriod
    #         close_series[maName] = maList
    #         print(close_series)
    #     maSeries = close_series.drop('tclose')
    #     return maSeries
        
                
            
        
    #6. 乖离BIAS, 指数下跌+ 低于大部分均线 + bias(24)<-10,叠加放量/ bias和量比之间的数量关系
    def build_bias_series(self,period):
        pass
    
    
    def get_vol_price_indicators(self,bar_data,begt,endt,fast_period,slow_period,dea_period,ma_fast_period,ma_slow_period):
        bar_data = bar_data.copy()
        
        #MACD动量指标
        bar_data['EMA_fast'] = bar_data['tclose'].ewm(alpha=2/(fast_period+1),adjust=False).mean()
        bar_data['EMA_slow'] = bar_data['tclose'].ewm(alpha=2/(slow_period+1),adjust=False).mean()
        bar_data['DIF'] = bar_data['EMA_fast'] - bar_data['EMA_slow']
        bar_data['DEA'] = bar_data['DIF'].ewm(alpha=2/(dea_period+1),adjust=False).mean()
        bar_data['MACD'] = 2 * (bar_data['DIF'] - bar_data['DEA'])
        
        #MA移动平均指标
        bar_data['MA_fast'] = bar_data['tclose'].rolling(ma_fast_period).mean()
        bar_data['MA_slow'] = bar_data['tclose'].rolling(ma_slow_period).mean()
        inds = bar_data[['trade_date','EMA_fast','EMA_slow','DIF','DEA','MACD','MA_fast','MA_slow']]
        # inds = inds[(inds['trade_date']>=begt) & (inds['trade_date']<=endt)]
        return inds
    
    