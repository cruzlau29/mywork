# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 13:57:31 2022

@author: liuyf
"""
from volpriceStrategyBacktestAlgo import VolPriceStrategyBacktestAlgo


if __name__=='__main__':
    #设置回测日期区间 
    begt = '2011-12-31'
    endt = '2022-05-31'
    # begt = '2012-02-07'
    # endt = '2022-05-31'
    
    #选择择时标的
    asset_code = '399006'
    
    #设置指标参数，填改要使用到的参数即可
    #macd动量参数
    fast_period = 12
    slow_period = 26
    dea_period = 9
    
    #ma均线动量参数
    ma_fast_period = 5
    ma_slow_period = 10
    
    #设置交易参考时间
    trade_time = 'open'
    
    
    algo = VolPriceStrategyBacktestAlgo(begt,endt,asset_code,fast_period,slow_period,dea_period,ma_fast_period,ma_slow_period,trade_time = trade_time)
    algo.generate_backtest_result()
